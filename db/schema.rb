# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_06_15_052417) do

  create_table "attendances", id: { type: :bigint, unsigned: true }, charset: "utf8", force: :cascade do |t|
    t.bigint "user_id", null: false, comment: "ユーザID", unsigned: true
    t.date "date", null: false, comment: "対象の日付"
    t.integer "status", default: 0, null: false, comment: "状態(1:下書き, 2:申請中 3:承認済 4:却下)", unsigned: true
    t.time "start_time", comment: "勤務開始時間"
    t.time "break_time", comment: "休憩時間"
    t.time "end_time", comment: "勤務終了時間"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at", null: false
    t.index ["deleted_at"], name: "index_attendances_on_deleted_at"
    t.index ["user_id"], name: "index_attendances_on_user_id"
  end

  create_table "users", id: { type: :bigint, unsigned: true }, charset: "utf8", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "deleted_at", null: false
    t.index ["deleted_at"], name: "index_users_on_deleted_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "attendances", "users"
end
