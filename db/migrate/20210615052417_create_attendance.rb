class CreateAttendance < Db::CreateBase
  def change
    create_table :attendances do |t|
      t.id      :user,                    comment: 'ユーザID'
      t.date    :date,       null: false, comment: '対象の日付'
      t.integer :status,     null: false, comment: '状態(1:下書き, 2:申請中 3:承認済 4:却下)', unsigned: true, default: 0
      t.time    :start_time, null: true,  comment: '勤務開始時間'
      t.time    :break_time, null: true,  comment: '休憩時間'
      t.time    :end_time,   null: true,  comment: '勤務終了時間'
    end
  end
end
