// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

import Rails from "@rails/ujs"
import Turbolinks from "turbolinks"
import * as ActiveStorage from "@rails/activestorage"
import "channels"

Rails.start()
Turbolinks.start()
ActiveStorage.start()

Date.prototype.format = function(format) {
	format = format.replace(/yyyy/g, this.getFullYear())
	format = format.replace(/mm/g, ('0' + (this.getMonth() + 1)).slice(-2))
	format = format.replace(/m/g, ('0' + (this.getMonth() + 1)).slice(-1))
	format = format.replace(/dd/g, ('0' + this.getDate()).slice(-2))
	format = format.replace(/d/g, ('0' + this.getDate()).slice(-1))
	format = format.replace(/hh/g, ('0' + this.getHours()).slice(-2))
	format = format.replace(/h/g, ('0' + this.getHours()).slice(-1))
	format = format.replace(/ii/g, ('0' + this.getMinutes()).slice(-2))
	format = format.replace(/i/g, ('0' + this.getMinutes()).slice(-1))
	format = format.replace(/ss/g, ('0' + this.getSeconds()).slice(-2))
	format = format.replace(/s/g, ('0' + this.getSeconds()).slice(-1))
	return format
}

document.fetch = (path, {body, method}) => {
	$('#loading').removeClass('d-none');
	method = method || 'GET';
	if (method == 'GET') path += `${path.match(/\?/) ? '&' : '?'}_=${new Date().getTime()}`;
	return fetch(path, {method: method, body: body}).then((resp, hoge) => {
		$('#loading').addClass('d-none');
		if (resp.status != 200) {
			const promise = new Promise(function(_, reject){
				resp.json().then((respJson) => {
					reject(respJson.error_code);
				})
			});
			return promise;
		}
		return resp.json();
	})
}

$(function(){
	if (location.pathname == '/') {
		function save(target, draft) {
			const body = new FormData(target);
			body.append('draft', draft ? '1' : '0');
			var url = $(target).attr('action');
			var method = 'POST';
			document.fetch(url, {method: method, body}).then((respJson) => {
				$('.form-group > *').removeClass('is-invalid');
				$('.form-control').removeClass('is-invalid');
				$('#errors').empty();
				$('#errors').addClass('d-none');
				var error_keys = Object.keys(respJson.errors);

				if (error_keys.length > 0) {
					var display_errors = false
					for (var i in respJson.errors) {
						var parent = $(`[name="${i}"]`).parents('.form-group');
						var input = parent.find('.invalid-feedback');
						input.empty();
						parent.find('> *').addClass('is-invalid');
						parent.find('.form-control').addClass('is-invalid');
						for (var j in respJson.errors[i]) {
							if (input.length == 0) {
								display_errors = true
								$('#errors').append(`${i}: ${respJson.errors[i][j]}<br>`)
							} else {
								input.append(`${respJson.errors[i][j]}<br>`);
							}
						}
					}
					if (display_errors) $('#errors').removeClass('d-none');
				}
				$('form .form-control').each(function(){
					if (draft) return;
					if (error_keys.includes($(this).attr('name'))) return;
					if ($(this).attr('disabled')) return;
					$(this).addClass('is-valid');
				});

				setRespJson(respJson);
			}).catch((error) =>{
				console.log(error);
			});
		}

		function setRespJson(respJson) {
			var attendance = respJson.attendance || {break_time: '01:00', status: 'draft'};
			respJson.user = respJson.user || {}
			$(`input[name="name"]`).val(respJson.user.email);
			var status = attendance.status;
			attendance.status = {
				draft:    '下書き',
				applying: '申請中',
				approved: '承認済',
				rejected: '却下',
			}[attendance.status]
			for (var i in attendance) {
				if (typeof(attendance[i]) == 'string') {
					var m = attendance[i].match(/T([0-9]{2}:[0-9]{2})/);
					if (m) attendance[i] = m[1];
				}
				$(`input[name="${i}"]`).val(attendance[i]);
			}
			$(`input[name="status"]`).data({status: status});
			$('[type="submit"]').prop('disabled', ['applying', 'approved'].includes(status))
			setShortCutBtnTitle();
		}

		function setDate() {
			$('form .form-control').each(function(){
				if (['date'].includes($(this).attr('name'))) return;
				$(this).val('');
			})

			var date = $('input[name="date"]').val();
			var url = `/?date=${date}`;
			if (date == (new Date()).format('yyyy-mm-dd')) url = '/';
			history.replaceState('', '', url);
			document.fetch(`/api/attendances/${date}`, {method: 'GET'}).then(setRespJson);
		}

		function mvDate(add) {
			var d = new Date($('input[name="date"]').val());
			d.setDate(d.getDate() + add);
			$('input[name="date"]').val(`${d.getFullYear()}-${('0' + (d.getMonth() + 1)).slice(-2)}-${('0' + d.getDate()).slice(-2)}`);
			setDate();
		}

		function setShortCutBtnTitle() {
			$('#short-cut-btn, #info').attr({class: 'd-none'});
			['start_time', 'end_time'].forEach(function(k){
				if ($('#short-cut-btn').attr('class') == 'd-block') return;
				var target = $(`[name="${k}"]`);
				if (target.val()) return;
				$('#short-cut-btn button')
					.text(target.data('btn-text'))
					.attr({class: `btn btn-${target.data('btn-type')} btn-lg btn-block my-4`});
				$('#short-cut-btn').attr({class: 'd-block'});
			});
			if ($('#short-cut-btn').attr('class') != 'd-none') return;

			var status = $(`input[name="status"]`).data('status');
			var text = {
				draft:    'まだ申請していません@danger',
				applying: '申請中です@warning',
				approved: '承認されました@success',
				rejected: '却下されました@danger',
			}[status]
			if (!text) return;

			var texts = text.split('@');
			$('#info').html(texts[0]).attr({class: `text-center alert alert-${texts[1]}`});
		}

		$('input, select').on('change', function(e){
			save($('form')[0], true);
		});

		$('form').on('submit', function(e){
			e.preventDefault();
			save(e.target, false);
		});

		$('[data-add]').click(function(e){
			e.preventDefault();
			var add = parseInt($(this).data('add'));
			mvDate(add);
		});

		$('input[name="date"]').change(setDate);

		$('#short-cut-btn button').on('click', function(e){
			e.preventDefault();
			var done = false;
			['start_time', 'end_time'].forEach(function(k){
				if (done) return;
				if ($(`[name="${k}"]`).val()) return;
				var d = new Date();
				d.setMinutes(Math.round(d.getMinutes() / 5) * 5);
				$(`[name="${k}"]`).val(d.format('hh:ii')).trigger('change');
				done = true;
			});
			setShortCutBtnTitle();
		})

		// $(window).keydown(function (e) {
		// 	if (e.code == 'ArrowLeft') {
		// 		mvDate(-1);
		// 	} else if (e.code == 'ArrowRight') {
		// 		mvDate(1);
		// 	}
		// })
		setDate();

	} else if (location.pathname == '/admin') {
		function submit(target, status) {
			var parent = $(target).parent('form');
			parent.find('input').val(status);
			$(parent).get(0).submit();
		}

		$(document).on('click', 'td:last-child .btn-danger, td:last-child .btn-success', function(e){
			e.preventDefault();
			if ($(this).hasClass('btn-success')) {
				submit(this, 'approved');
			} else if ($(this).hasClass('btn-danger')) {
				submit(this, 'rejected');
			}
		});
	}
});
