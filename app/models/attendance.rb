class Attendance < ApplicationRecord
  enum status: %i[draft applying approved rejected]
  belongs_to :user

  validates :date, presence: true
  validates :start_time, presence: true
  validates :break_time, presence: true
  validates :end_time, presence: true

  def status_ja
    return '未作成' if id.blank?

    {
      'draft'    => '下書き',
      'applying' => '申請中',
      'approved' => '承認済',
      'rejected' => '却下',
    }[status]
  end

  def start_time_str
    time_str start_time
  end

  def end_time_str
    time_str end_time
  end

  def break_time_str
    time_str break_time
  end

  def sum_time_str
    return '-' unless start_time.present? && end_time.present? && break_time.present?

    min = ((end_time - start_time) / 60) - break_time.hour * 60 + break_time.min
    "#{(min / 60).to_i.to_s.rjust(2, '0')}:#{(min % 60).to_i.to_s.rjust(2, '0')}"
  end

  private

  def time_str(t)
    return '-' if t.blank?

    t.strftime '%H:%M'
  end
end
