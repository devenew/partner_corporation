class Api::AttendancesController < Api::ApplicationController
  def index
    rows = Attendance.all
    render_json({attendances: rows, user: current_user})
  end

  def show
    row = attendance params[:id]
    render_json({attendance: row, user: current_user})
  end

  def create
    row = attendance attendance_params[:date]
    if row.present?
      row.assign_attributes attendance_params
    else
      row = Attendance.new attendance_params
      current_user.attendances << row
    end

    not_use_err = draft? && row.id.blank?
    row.status = draft? ? :draft : :applying
    row.save(validate: !draft?)

    render_json({errors: (not_use_err ? {} : row.errors.messages), attendance: row, user: current_user}, status: 200)
  end

  private

  def draft?
    params[:draft] == '1'
  end

  def attendance_params
    params.permit %i[date start_time break_time end_time]
  end

  def attendance(date)
    current_user.attendances.find_by date: date
  end
end
