class Api::ApplicationController < ApplicationController
	skip_before_action :verify_authenticity_token

	before_action :require_login

	private

	def render_error_json(code = nil, status: 500)
		render_json({error_code: code || status}, status: status)
	end

	def render_json(json, status: 200)
		render status: status, json: json
	end

	def require_login
		render_error_json(status: 401) unless user_signed_in?
	end
end
