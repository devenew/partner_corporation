class ApplicationController < ActionController::Base
  DATE_FORMAT = '%Y-%m-%d'

  layout 'service'

  protected

  def require_login
    return if user_signed_in?

    flash[:error] = "You must be logged in to access this section"
    redirect_to login_url
  end
end
