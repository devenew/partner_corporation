class TopController < ApplicationController
	before_action :require_login

	def index
		@date = date
	end

	private

	def date
		Date.strptime params[:date], DATE_FORMAT
	rescue
		Date.today
	end
end
