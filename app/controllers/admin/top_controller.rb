class Admin::TopController < Admin::ApplicationController
	def index
		@date = date

		attendances = Attendance.where(date: @date).map do |row|
			[row.user_id, row]
		end.to_h

		@rows = User.all.map(&:attributes).map(&:with_indifferent_access).map do |row|
			row[:attendance] = attendances[row[:id]] || Attendance.new
			row
		end
	end

	def update
		attendance = Attendance.find params[:id]
		attendance.update! status: attendance_params[:status]

		redirect_to action: :index, date: attendance.date.strftime(DATE_FORMAT)
	end

	private

	def attendance_params
		params.permit %i[status]
	end

	def date
		Date.strptime params[:date], DATE_FORMAT
	rescue
		Date.today
	end
end
