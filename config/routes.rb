Rails.application.routes.draw do
  devise_for :users, controllers: {
    registrations: 'users/registrations',
    sessions: 'users/sessions'
  }

  devise_scope :user do
    get :signup, to: 'users/registrations#new'
    get :login,  to: 'users/sessions#new'
    get :logout, to: 'users/sessions#destroy'
  end

  root 'top#index'

  namespace :admin do
    root 'top#index'
    post '/:id', to: 'top#update'
  end

  namespace :api do
    resources :attendances
    namespace :admin do
      resources :attendances
    end
  end

end
